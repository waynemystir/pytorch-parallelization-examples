#!/bin/bash

if [ ! -d "./data" ]
then
    mkdir data
fi

cd data
wget www.di.ens.fr/~lelarge/MNIST.tar.gz
tar -zxvf MNIST.tar.gz
