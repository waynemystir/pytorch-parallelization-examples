from pathlib import Path
import argparse
import sys

import torch
from torch.utils.data import DataLoader

import ddp_mnist


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-v',
        '--version',
        default=0,
        type=str,
        help='version',
    )
    args = parser.parse_args()
    assert isinstance(args.version, str), f"{args.version=}"
    version_path = Path.cwd() / "output" / args.version
    if not version_path.exists():
        raise ValueError(f"No version found at {str(version_path)}.")

    artifacts_path = version_path / "artifacts.pt"
    assert artifacts_path.exists(), f"No artifacts found at {str(artifacts_path)}."

    ml = torch.device("cpu")
    artifacts = torch.load(artifacts_path, map_location=ml)

    train_ds = artifacts["train_dataset"]
    val_ds = artifacts["val_dataset"]
    if train_ds is None and val_ds is None:
        sys.exit(f"No files found at {tdsfp=} or {vdsfp=}. Exiting.")

    model_state_dict = artifacts["model_state_dict"]
    assert isinstance(model_state_dict, dict), f""
    model = ddp_mnist.ConvNet()
    model.load_state_dict(model_state_dict)

    val_loader = DataLoader(
        dataset=val_ds,
        batch_size=len(val_ds),
#        batch_size=args.batch_size,
        shuffle=False,
        num_workers=32,
#        pin_memory=True,
    )

    print(f"Start")
    for x, y in val_loader:
        o = model(x)
        am = o.argmax(-1)

    correct = am == y
    accuracy = (correct * 1.).mean()
    print(f"Validation {accuracy=:.4f}")
    import pdb; pdb.set_trace()


if __name__ == "__main__":
    main()
