import os
import logging
from datetime import datetime
import argparse
from pathlib import Path

import torch.multiprocessing as mp
import torchvision
import torchvision.transforms as transforms
import torch
import torch.nn as nn
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data.distributed import DistributedSampler
from torch.utils.data import random_split, DataLoader
from torchvision.datasets import MNIST as MNIST_ds

import utilities


def reduce_mean(tensor, rank, world_size):
    with torch.no_grad():
        dist.reduce(tensor, dst=0, op=dist.ReduceOp.SUM)
        if rank == 0:
            tensor /= world_size


def save_training_artifacts(
    model,
    args,
    train_dataset,
    val_dataset,
    save_dir,
):
    if not (isinstance(save_dir, Path) or isinstance(save_dir, str)):
        raise TypeError(f"")
    if isinstance(save_dir, str):
        save_dir = Path(save_dir)
    save_dir.mkdir(parents=True, exist_ok=True)
    save_path = save_dir / "artifacts.pt"

    raw_model = model.module if hasattr(model, "module") else model

    state = {
        "args": args,
        "model_state_dict": raw_model.state_dict(),
        "train_dataset": train_dataset,
        "val_dataset": val_dataset,
    }

    logging.warning(f"Saving model to {str(save_path)}")
    torch.save(state, save_path)


class ConvNet(nn.Module):
    def __init__(self, num_classes=10):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.fc = nn.Linear(7*7*32, num_classes)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return out


def spawn_process(gpu, args, log_queue):

    def run_epoch(dataloader, is_train=True):
        model.train(is_train)
        epoch_items = torch.tensor(0).cuda()
        epoch_loss = torch.tensor(.0).cuda()
        epoch_preds = torch.empty(0).cuda()
        epoch_labels = torch.empty(0).cuda()
        epoch_accuracy = torch.tensor(.0).cuda()
        num_batches = len(dataloader)
        for i, (images, labels) in enumerate(dataloader):
            images = images.cuda(non_blocking=True)
            labels = labels.cuda(non_blocking=True)
            epoch_labels = torch.cat((epoch_labels, labels), dim=0)
            epoch_items += images.size(0)

            # Forward pass
            outputs = model(images)
            preds = outputs.argmax(-1)
            epoch_preds = torch.cat((epoch_preds, preds), dim=0)
            loss = criterion(outputs, labels)
            epoch_loss += loss

            # Backward and optimize
            if is_train:
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

        epoch_loss /= epoch_items
        rl = epoch_loss.detach().clone()
        # usesless tensor to prevent pytorch from locking during "if gpu!=0: assert torch.equal(..)"
#        qaz = torch.tensor([1.]).cuda()
        reduce_mean(epoch_loss, gpu, args.num_gpus)

        epoch_accuracy = ((epoch_preds == epoch_labels) * 1.0).mean()
        ra = epoch_accuracy.detach().clone()
        reduce_mean(epoch_accuracy, gpu, args.num_gpus)

        if gpu != 0:
            assert torch.equal(epoch_loss, rl)
            assert torch.equal(epoch_accuracy, ra)
        return epoch_loss.item(), epoch_accuracy.item()

#    if gpu == 0:
    utilities.setup_worker_logging(gpu, log_queue)

    # nccl is currently the fastest backend:
    # https://github.com/pytorch/pytorch/blob/f008e8d32d758115c978352e002d86a1faf2f18a/torch/nn/parallel/distributed.py#L250-L252
    #
    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '12684'
#    dist.init_process_group(backend='nccl', init_method='env://', world_size=args.num_gpus, rank=gpu)
    dist.init_process_group(backend='nccl', world_size=args.num_gpus, rank=gpu)
    torch.manual_seed(args.rand_seed)
    model = ConvNet()

    # Set one process per GPU:
    # https://github.com/pytorch/pytorch/blob/f008e8d32d758115c978352e002d86a1faf2f18a/torch/nn/parallel/distributed.py#L221-L234
    #
    torch.cuda.set_device(gpu)
    model.cuda(gpu)
    # Wrap the model in SyncBatchNorm. It's unclear to me if we should do this but I think it's unnecessary and not helpful.
    #
    # From experimentation, it's seems the loss and accuracy numbers are about the same whether we use this or not. But using
    # SyncBatchNorm clearly and substantially slows down training. I guess the response by Pieter in the first link indicates
    # that we do not need to use BN sync:
    #
    # https://discuss.pytorch.org/t/do-nn-batchnorm-in-distributed-training-default-to-be-synchronized/42140
    # https://github.com/pytorch/pytorch/blob/4e347f12422aad80e03ae722429a828ee864b13a/torch/nn/modules/batchnorm.py#L549-L819
    # https://github.com/pytorch/pytorch/blob/2733555ed1fa9b32cb23ee8d387f968b582de5f3/torch/nn/modules/_functions.py#L6
    #
#    model = nn.SyncBatchNorm.convert_sync_batchnorm(model)

    # Wrap the model in DDP
    #
    model = DDP(model, device_ids=[gpu])

    # define loss function (criterion) and optimizer
    criterion = nn.CrossEntropyLoss(reduction="sum").cuda(gpu)
    # TODO: Do I need to wrap the optimizer in DistributedOptimizer?
    #
    # The optimizer appears to work across all GPU's well without it. But I see an example that uses it:
    # https://github.com/pytorch/examples/blob/master/distributed/rpc/ddp_rpc/main.py#L63-L67
    #
    # Unfortunately, the documentation is abstruse, unclear, and vague:
    # https://pytorch.org/docs/master/rpc.html#torch.distributed.optim.DistributedOptimizer
    #
    # It might be inferred from this comment that DistributedOptimizer is intended to be
    # used only coupling DDP with RPC, which we're not doing here:
    # https://github.com/pytorch/pytorch/blob/f008e8d32d758115c978352e002d86a1faf2f18a/torch/nn/parallel/distributed.py#L283-L288
    optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate)

    # Data loading
    assert Path("./data/MNIST").exists(), "The PyTorch MNIST dataset usually causes difficulties when attempting to download. Run the script get-mnist-data.sh before running this script."
    mnist_dataset = MNIST_ds(
        root='./data',
        train=True,
        transform=transforms.ToTensor(),
        download=False,
    )
    assert len(mnist_dataset) == 60000
    train_dataset, val_dataset = random_split(mnist_dataset, [50000, 10000])
#    tdsfp = args.output_dir / f"train-dataset.pt"
#    vdsfp = args.output_dir / f"val-dataset.pt"
#    if not tdsfp.exists():
#        torch.save(train_dataset, tdsfp)
#    if not vdsfp.exists():
#        torch.save(val_dataset, vdsfp)

    train_sampler = DistributedSampler(
        train_dataset,
        num_replicas=args.num_gpus,
        rank=gpu,
    )
    # If I set num_workers to a positive integer, training slows to a crawl.
    train_loader = DataLoader(
        dataset=train_dataset,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=0,
        pin_memory=True,
        sampler=train_sampler,
    )

    val_sampler = DistributedSampler(
        val_dataset,
        num_replicas=args.num_gpus,
        rank=gpu,
    )
    val_loader = DataLoader(
        dataset=val_dataset,
        batch_size=len(val_dataset),
#        batch_size=args.batch_size,
        shuffle=False,
        num_workers=0,
        pin_memory=True,
        sampler=val_sampler,
    )

    # We call _sampler.set_epoch(epoch) for train and val below BEFORE calling run_epoch() because of
    # this warning:
    #
    # https://github.com/pytorch/pytorch/blob/f008e8d32d758115c978352e002d86a1faf2f18a/torch/utils/data/distributed.py#L42-L57

    start = datetime.now()
    for epoch in range(1, args.epochs + 1):
        train_sampler.set_epoch(epoch)
        train_loss, train_acc = run_epoch(train_loader)
        val_sampler.set_epoch(epoch)
        val_loss, val_acc = run_epoch(val_loader, is_train=False)
        if gpu == 0:
            logging.warning(f"{gpu=} Epoch [{epoch}/{args.epochs}], {train_loss=:.4f}, {train_acc=:.4f}, {val_loss=:.4f}, {val_acc=:.4f}")

    dist.destroy_process_group()
    if gpu == 0:
        logging.warning("Saving artifacts...")
        save_training_artifacts(
            model,
            args,
            train_dataset,
            val_dataset,
            args.output_dir,
        )
        logging.warning("Training complete in: " + str(datetime.now() - start))


def main():
    mp.set_start_method("spawn", force=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--num_gpus', default=4, type=int,
                        help='number of num_gpus per node')
    parser.add_argument('-e', '--epochs', default=10, type=int, metavar='N',
                        help='number of total epochs to run')
    parser.add_argument('-b', '--batch-size', default=100, type=int, metavar='N',
                        help='batch size')
    parser.add_argument('-lr', '--learning-rate', default=1e-3, type=float,
                        help='learning rate')
    parser.add_argument('--rand_seed', default=0, type=int, metavar='N',
                        help='random seed')
    args = parser.parse_args()
    assert isinstance(args.rand_seed, int), f"{args.rand_seed=}"

    if isinstance(args.rand_seed, int):
        from torch import manual_seed, cuda, backends
        from numpy import random as npr
        import random
        manual_seed(args.rand_seed)
        cuda.manual_seed(args.rand_seed)
        npr.seed(args.rand_seed)
        random.seed(args.rand_seed)
        backends.cudnn.enabled = False
        backends.cudnn.deterministic = True

    args.output_dir = utilities.get_output_dir()
    log_queue = utilities.setup_primary_logging()

    logging.warning(f"Start spawning with {args.num_gpus=} {args.rand_seed=} {args.batch_size=} {args.learning_rate=} for {args.epochs=} ...")
    mp.spawn(spawn_process, nprocs=args.num_gpus, args=(args, log_queue), join=True)
    # I don't understand what "join" means in this context. I found the docs to be confusing.
#    mp.spawn(spawn_process, nprocs=args.num_gpus, args=(args, ))
    logging.warning(f"DONE")


if __name__ == '__main__':
    main()
