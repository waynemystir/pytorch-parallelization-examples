from pathlib import Path
import sys
import logging
from logging.handlers import QueueHandler, QueueListener

import torch.multiprocessing as torch_mp


OUTPUT_DIR = Path.cwd() / "output"
OUTPUT_VERS_DIRNAME_LEN = 4

OUTPUT_DIR.mkdir(parents=True, exist_ok=True)


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


def get_output_dir():
    av = get_output_version()
    ad = OUTPUT_DIR / av
    if not ad.exists():
        ad.mkdir(parents=True)
    return ad


@static_vars(output_version=-1)
def get_output_version():
    if not isinstance(get_output_version.output_version, str):
        max_dir_num = -1
        subdirs = [str(item.stem) for item in OUTPUT_DIR.iterdir() if item.is_dir()]
        print(f'Looking in {str(OUTPUT_DIR)} for new output directory version.')
        for dn in subdirs:
            try:
                n = int(dn)
            except ValueError:
                continue
            max_dir_num = max(max_dir_num, n)

        get_output_version.output_version = str(max_dir_num + 1).zfill(OUTPUT_VERS_DIRNAME_LEN)
        print(f'Selected {get_output_version.output_version} for the new output directory version.')

    return get_output_version.output_version


"""
logging code take from here:

https://gist.github.com/scarecrow1123/967a97f553697743ae4ec7af36690da6
"""
def setup_primary_logging() -> torch_mp.Queue:
    """
    Global logging is setup using this method. In a distributed setup, a multiprocessing queue is setup
    which can be used by the workers to write their log messages. This initializers respective handlers
    to pick messages from the queue and handle them to write to corresponding output buffers.
    Parameters
    ----------
    log_file_path : ``str``, required
        File path to write output log
    error_log_file_path: ``str``, required
        File path to write error log
    Returns
    -------
    log_queue : ``torch.multiprocessing.Queue``
        A log queue to which the log handler listens to. This is used by workers
        in a distributed setup to initialize worker specific log handlers(refer ``setup_worker_logging`` method).
        Messages posted in this queue by the workers are picked up and bubbled up to respective log handlers.
    """
    # Multiprocessing queue to which the workers should log their messages
    log_queue = torch_mp.Queue(-1)

    log_file_path = get_output_dir() / "main.log"
    error_log_file_path = get_output_dir() / "error.log"

    # Handlers for stream/file logging
    output_file_log_handler = logging.FileHandler(filename=str(log_file_path))
    error_file_log_handler = logging.FileHandler(filename=str(error_log_file_path))

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    output_file_log_handler.setFormatter(formatter)
    error_file_log_handler.setFormatter(formatter)

    output_file_log_handler.setLevel(logging.INFO)
    error_file_log_handler.setLevel(logging.ERROR)
    logging.getLogger().addHandler(output_file_log_handler)
    logging.getLogger().addHandler(error_file_log_handler)

    out_handler = logging.StreamHandler(sys.stdout)
    out_handler.setFormatter(formatter)
    out_handler.setLevel(logging.INFO)
    logging.getLogger().addHandler(out_handler)

    err_handler = logging.StreamHandler(sys.stderr)
    err_handler.setFormatter(formatter)
    err_handler.setLevel(logging.ERROR)
    logging.getLogger().addHandler(err_handler)

    # This listener listens to the `log_queue` and pushes the messages to the list of
    # handlers specified.
    listener = QueueListener(log_queue, output_file_log_handler, error_file_log_handler, out_handler, err_handler, respect_handler_level=True)

    listener.start()

    return log_queue


class WorkerLogFilter(logging.Filter):
    def __init__(self, rank=-1):
        super().__init__()
        self._rank = rank

    def filter(self, record):
        if self._rank != -1:
            record.msg = f"Rank {self._rank} | {record.msg}"
        return True


def setup_worker_logging(rank: int, log_queue: torch_mp.Queue):
    """
    Method to initialize worker's logging in a distributed setup. The worker processes
    always write their logs to the `log_queue`. Messages in this queue in turn gets picked
    by parent's `QueueListener` and pushes them to respective file/stream log handlers.
    Parameters
    ----------
    rank : ``int``, required
        Rank of the worker
    log_queue: ``Queue``, required
        The common log queue to which the workers
    Returns
    -------
    features : ``np.ndarray``
        The corresponding log power spectrogram.
    """
    queue_handler = QueueHandler(log_queue)

    # Add a filter that modifies the message to put the
    # rank in the log format
    worker_filter = WorkerLogFilter(rank)
    queue_handler.addFilter(worker_filter)

    queue_handler.setLevel(logging.INFO)

    root_logger = logging.getLogger()
    root_logger.addHandler(queue_handler)

    # Default logger level is WARNING, hence the change. Otherwise, any worker logs
    # are not going to get bubbled up to the parent's logger handlers from where the
    # actual logs are written to the output
    root_logger.setLevel(logging.INFO)
